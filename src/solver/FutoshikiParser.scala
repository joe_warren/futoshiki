package solver
import scala.util.parsing.combinator._

class FutoshikiParser extends RegexParsers {
  def definiteValue: Parser[Candidates] =
    """[0-9]""".r ^^
      (x => List(x.toInt));
  def indefiniteValue: Parser[Candidates] =
    "." ^^
      (x => List());
  def value: Parser[Candidates] = definiteValue | indefiniteValue
  def coordinatePart: Parser[Int] =
    """[0-9]""".r ^^
      { _.toInt };
  def coordinate: Parser[Coordinate] =
    "(" ~ coordinatePart ~ "," ~ coordinatePart ~ ")" ^^
      { case op ~ x ~ c ~ y ~ cl => Tuple2(x, y) };
  def constraint: Parser[Inequality] =
    "(" ~ coordinate ~ "<" ~ coordinate ~ ")" ^^
      { case op ~ less ~ c ~ more ~ cl => (less, more) };
  def constraints: Parser[Array[Inequality]] =
    rep(constraint) ^^
      (x => x.toArray);
  def grid: Parser[Grid] =
    rep(value) ^^
      { ga =>
        {
          val length = Math.sqrt(ga.length).toInt;
          if (length * length != ga.length) {
            throw new Exception("Fotoshiki Grid is not square")
          }
          val gridArray = ga.map(x =>
            if (x.length == 0)
              1 to length toList
            else
              x);
          val grid = gridArray.zipWithIndex.map(x => 
            Tuple2(Tuple2(x._2 / length, x._2 % length), x._1)).toMap;
          grid
        }
      }
  def definition: Parser[(Grid, Array[Inequality])] =
    (grid ~ ":" ~ constraints) ^^
      { case x ~ sep ~ y => (x, y) }
}