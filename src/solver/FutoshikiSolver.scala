package solver

class FutoshikiSolver {

  def rowIndices(j: Int, len: Int): Array[Coordinate] = {
    val is: Array[Int] = 0 to len - 1 toArray;
    is.map(i => Tuple2(i, j))
  }
  def columnIndices(i: Int, len: Int): Array[Coordinate] = {
    val js: Array[Int] = 0 to len - 1 toArray;
    js.map(j => Tuple2(i, j))
  }

  def candidateLengthString(grid: Grid): String = {
    val length = gridLength(grid);
    val is: List[Int] = 0 to length - 1 toList;
    is.foldLeft("")((str, i) => {
      val js: List[Int] = 0 to length - 1 toList;
      str + js.foldLeft("")((s, j) => s + grid((i, j)).length.toString) + "\n"
    })
  }
  def updateGridWithInequality(grid: Grid, inequality: Inequality, inequalities: Array[Inequality]): Grid = {
    val min = grid(inequality._1);
    val max = grid(inequality._2);
    val newMin = min.filter(x => x < max.last);
    val newMax = max.filter(x => x > min(0));
    val newGrid = grid.updated(inequality._1, newMin).updated(inequality._2, newMax);
    val toPropogate = Array(inequality._1, inequality._2);
    val propogated = toPropogate.foldLeft[Grid](newGrid)((g, co) =>
      if (newGrid(co).length == 1 && grid(co).length != 1)
        propogateConstraints(co, g, inequalities)
      else
        g);
    propogated
  }
  def propogateConstraints(co: Coordinate, grid: Grid, inequalities: Array[Inequality]): Grid = {
    val length = gridLength(grid);
    val value = grid(co)(0);
    val constrained = (columnIndices(co._1, length) ++ rowIndices(co._2, length)).filter(_ != co);
    val updates = constrained.map(x => (x, grid(x).filter(_ != value)));
    val updated = updates.foldLeft[Grid](grid)((g, u) => g.updated(u._1, u._2));
    val toPropogate = constrained.filter(c => updated(c).length == 1 && grid(c).length != 1);
    val propogated = constrained.foldLeft[Grid](updated)((g, c) =>
      if (toPropogate.contains(c))
        propogateConstraints(c, g, inequalities)
      else
        g);
    val changed = constrained.filter(c => updated(c).length < grid(c).length);
    val relevantInequalities = inequalities.filter(x => changed.contains(x._1) || changed.contains(x._2));
    relevantInequalities.foldLeft[Grid](propogated)((g, ieq) => updateGridWithInequality(g, ieq, inequalities))
  }
  def isPotentiallyValid(grid: Grid, inequalities: Array[Inequality]): Boolean = {
    val length = gridLength(grid);
    val singularValues = grid.toArray.filter(x => x._2.length == 1).map(x => x._1);
    val collisions = singularValues.foldLeft[Int](0)((col, co) => {
      val value = grid(co)(0);
      val constrained = (rowIndices(co._2, length) ++ columnIndices(co._1, length)).filter(_ != co);
      col + constrained.map(x => grid(x)).filter(x => x.length == 1 && x(0) == value).length
    });
    val inequalitiesOk = inequalities.foldLeft[Boolean](true)((ok, ie) =>
      ok && (grid(ie._1)(0) < grid(ie._2).last))
    ((collisions == 0) && inequalitiesOk)
  }
  def isSolved(grid: Grid): Boolean = {
    grid.toArray.filter(x => (x._2.length != 1)).length == 0
  }
  def recurse(grid: Grid, inequalities: Array[Inequality]): Grid = {

    if (isSolved(grid) && isPotentiallyValid(grid, inequalities)) {
      return grid;
    }
    val mincandidates = grid.toArray
      .sortWith((x, y) => x._2.length < y._2.length)
      .filter(x => (x._2.length != 1))(0);
    for (candidate <- mincandidates._2) {
      val newgrid = grid.updated(mincandidates._1, List(candidate));
      val propogated = propogateConstraints(mincandidates._1, newgrid, inequalities);
      val recursed = recurse(propogated, inequalities);
      if (isSolved(recursed) && isPotentiallyValid(recursed, inequalities)) {
        return recursed
      }
    }
    grid
  }
  def solve(grid: Grid, inequalities: Array[Inequality]): Grid = {
    val singularValues = grid.toArray.filter(x => x._2.length == 1).map(x => x._1);
    val propogatedWithCon = singularValues.foldLeft[Grid](grid)((g, co) => 
      propogateConstraints(co, g, inequalities))
    val propogatedWithIneq = inequalities.foldLeft[Grid](propogatedWithCon)((g, ieq) =>
      updateGridWithInequality(g, ieq, inequalities))
    recurse(propogatedWithIneq, inequalities)
  }
}