package solver

object FutoshikiProgram {
  def formatPrintGrid(grid: Grid): String = {
    val length = gridLength(grid);
    val is: List[Int] = 0 to length - 1 toList;
    is.foldLeft("")((str, i) => {
      val js: List[Int] = 0 to length - 1 toList;
      str + js.foldLeft("")((s, j) => s + (
        if (grid((i, j)).length == 1)
          grid((i, j))(0).toString
        else
          ".")
      ) + "\n"
    })
  }
  def formatPrintInequalities(inequalities: Array[Inequality]): String = {
    inequalities.foldLeft[String]("")(
      (acc, ie) =>
        acc + "(" + ie._1._1.toString + "," + ie._1._2.toString +
          ")<(" + ie._2._1.toString + "," + ie._2._2.toString + ")\n")
  }
  def main(args: Array[String]) = {
    val parser = new FutoshikiParser();
    val solver = new FutoshikiSolver();
    for (file <- args) {
      val string = scala.io.Source.fromFile(file).mkString;

      parser.parseAll(parser.definition, string) match {
        case parser.Success(res, _) => {
          val (grid, inequalities) = res;
          println(formatPrintGrid(grid));
          println(formatPrintInequalities(inequalities));

          val solved = solver.solve(grid, inequalities);
          println(formatPrintGrid(solved));
        }
        case x => println(x)
      }
    }
  }
}