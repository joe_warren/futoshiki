

package object solver {
  type Coordinate = Tuple2[Int, Int];
  type Candidates = List[Int];
  type Grid = Map[Coordinate, Candidates ];
  type Inequality = Tuple2[ Coordinate, Coordinate ];

  def gridLength(grid: Grid): Int = {
    Math.sqrt(grid.keys.toList.length).toInt
  }
}